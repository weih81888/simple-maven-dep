package com.example.dep;

import java.io.File;
import java.io.IOException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * Unit test for simple Dep.
 */
public class DepTest extends TestCase
    {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DepTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( DepTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testDep_1()
    {
      System.out.println("=================");
      System.out.println("Print out test 1");
      System.out.println("=================");
      assertTrue( true );
    }

    public void testDep_2()
    {
      System.out.println("=================");
      System.out.println("Print out test 2");
      System.out.println("=================");
      assertTrue( true );
    }

    public void testSendGet()
    {
      System.out.println("======================================================================");
      System.out.println("PIPELINE - API TEST DEMO");
      System.out.println("======================================================================");
      //--https://18faddressservice.azurewebsites.net
      //--/api/addressbook/contacts/1000000000000001729/entries
      RestAssured.baseURI = "https://18faddressservice.azurewebsites.net";
      RequestSpecification httpRequest = RestAssured.given();
      Response response = httpRequest.request(Method.GET, "/api/addressbook/contacts/1000000000000001729/entries");
      String responseBody = response.getBody().asString();
      // Debug only
      //System.out.println("Response Body is =>  " + responseBody);
      JsonPath jsonPathEvaluator = response.jsonPath();
      int totalNumberofEntries = jsonPathEvaluator.get("Result.TotalEntries");
      System.out.println("==============================================");
  		System.out.println("TotalNumberofAddressBookEntries =>  " + totalNumberofEntries);
      // Debug only
      System.out.println("Entry ID: "+jsonPathEvaluator.get("Result.AddressBookEntries.AddressBookEntryId"));
      System.out.println("==============================================");
      for(int i=0; i<totalNumberofEntries; i++ )
      {
            int 	AddressBookEntryId = jsonPathEvaluator.get("Result.AddressBookEntries.AddressBookEntryId["+i+"]");
            long 	ContactId = jsonPathEvaluator.get("Result.AddressBookEntries.ContactId["+i+"]");
            String 	NamePrefix = jsonPathEvaluator.get("Result.AddressBookEntries.NamePrefix["+i+"]");
            String 	FirstName = jsonPathEvaluator.get("Result.AddressBookEntries.FirstName["+i+"]");
            String 	MiddleInitial = jsonPathEvaluator.get("Result.AddressBookEntries.MiddleInitial["+i+"]");
            String 	NameSuffix = jsonPathEvaluator.get("Result.AddressBookEntries.NameSuffix["+i+"]");
            String 	RelationShip = jsonPathEvaluator.get("Result.AddressBookEntries.RelationShip["+i+"]");
            String 	NickName = jsonPathEvaluator.get("Result.AddressBookEntries.NickName["+i+"]");
            String 	EmailAddress = jsonPathEvaluator.get("Result.AddressBookEntries.EmailAddress["+i+"]");
            String 	LastUpdateDate = jsonPathEvaluator.get("Result.AddressBookEntries.LastUpdateDate["+i+"]");
            String 	PhoneNumber = jsonPathEvaluator.get("Result.AddressBookEntries.PhoneNumber["+i+"]");
            String 	AlternatePhoneNumber = jsonPathEvaluator.get("Result.AddressBookEntries.AlternatePhoneNumber["+i+"]");
            String 	EmployerName = jsonPathEvaluator.get("Result.AddressBookEntries.EmployerName["+i+"]");
            int 	NumberOfAddresses = jsonPathEvaluator.get("Result.AddressBookEntries.NumberOfAddresses["+i+"]");
            String 	DateOfBirth = jsonPathEvaluator.get("Result.AddressBookEntries.DateOfBirth["+i+"]");
            System.out.println("================ Iteration # "+(i+1)+" Start ==================================");
            System.out.println("================ AddressBookEntryId: "+AddressBookEntryId);
            System.out.println("================ ContactId: "+ContactId);
            System.out.println("================ NamePrefix: "+NamePrefix);
            System.out.println("================ FirstName: "+FirstName);
            System.out.println("================ MiddleInitial: "+MiddleInitial);
            System.out.println("================ NameSuffix: "+NameSuffix);
            System.out.println("================ RelationShip: "+RelationShip);
            System.out.println("================ NickName: "+NickName);
            System.out.println("================ EmailAddress: "+EmailAddress);
            System.out.println("================ LastUpdateDate: "+LastUpdateDate);
            System.out.println("================ MiddleInitial: "+MiddleInitial);
            System.out.println("================ PhoneNumber: "+PhoneNumber);
            System.out.println("================ AlternatePhoneNumber: "+AlternatePhoneNumber);
            System.out.println("================ EmployerName: "+EmployerName);
            System.out.println("================ NumberOfAddresses: "+NumberOfAddresses);
            System.out.println("================ DateOfBirth: "+DateOfBirth);
            System.out.println("================ Iteration End ============================================");
      }
    }
}
